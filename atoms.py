import numpy as np
class atoms():
    def __init__(self,object_name = 'none'): # The second argument is the atoms object instance that will be operated.
        if object_name == 'none':
            print("Pass an object_name when initiating an atoms  object instance.")
            return
        self.total = 0.0
        self.symbols = []
        self.numbers = [] # These will be set and will print an error if they are not set.
        self.elements_of_atoms = []
        self.atoms_pos_angstroms = []
        self.periodic_images_positive_a = []
        self.periodic_images_negative_a = []
        self.periodic_images_positive_b = []
        self.periodic_images_negative_b = []
        self.periodic_images_pos_a_pos_b = []
        self.periodic_images_pos_a_neg_b = []
        self.periodic_images_neg_a_pos_b = []
        self.periodic_images_neg_a_neg_b = []

        def __del__(self):
            print('deleted')

    def read_CONTCAR(self, filename = ''):
        if filename == '':
            print("Please pass the filename of a CONTCAR file as a string to this function.")
        print(filename)
        f = open(filename, "r")
        line1 = f.readline()
        lattice_constant = float(f.readline())
        xx = [float(x) for x in f.readline().split()]
        yy = [float(y) for y in f.readline().split()]
        zz = [float(z) for z in f.readline().split()]
        cell = np.array([xx, yy, zz]) * lattice_constant
        symbols = f.readline().split()
        numbers = [int(n) for n in f.readline().split()]
        total = sum(numbers)
        atomic_formula = ''.join('{:s}{:d}'.format(sym, numbers[n]) for n, sym in enumerate(symbols))
        selective_dynamics = f.readline() # Move past this string towards the atom coordinates.
        direct_or_cartesian = f.readline() # Another string line.
        atoms_pos = np.empty((total, 3))
        selective_flags = np.empty((total, 3), dtype=bool)
        for atom in range(total):
            ac = f.readline().split()
            atoms_pos[atom] = (float(ac[0]), float(ac[1]), float(ac[2]))
            curflag = []
            for flag in ac[3:6]:
                curflag.append(flag == 'F')
            selective_flags[atom] = curflag
        if direct_or_cartesian.strip() == "Direct": 
            self.atoms_pos_angstroms = np.matmul(atoms_pos,cell) # Now 'atoms_pos_angstroms' is an attribute of this object.
            print("It got here for Direct.")
        elif direct_or_cartesian.strip() == "Cartesian":
            self.atoms_pos_angstroms = np.asarray(atoms_pos)
        else:
            print("Could not read POSCAR.")
        self.periodic_images_positive_a = self.atoms_pos_angstroms + np.tile(np.array(xx)*lattice_constant,(len(self.atoms_pos_angstroms),1))
        self.periodic_images_negative_a = self.atoms_pos_angstroms - np.tile(np.array(xx)*lattice_constant,(len(self.atoms_pos_angstroms),1))
        self.periodic_images_positive_b = self.atoms_pos_angstroms + np.tile(np.array(yy)*lattice_constant,(len(self.atoms_pos_angstroms),1))
        self.periodic_images_negative_b = self.atoms_pos_angstroms - np.tile(np.array(yy)*lattice_constant,(len(self.atoms_pos_angstroms),1))
        self.periodic_images_pos_a_pos_b = self.atoms_pos_angstroms + np.tile(np.array(xx)*lattice_constant,(len(self.atoms_pos_angstroms),1)) + np.tile(np.array(yy)*lattice_constant,(len(self.atoms_pos_angstroms),1))
        self.periodic_images_pos_a_neg_b = self.atoms_pos_angstroms + np.tile(np.array(xx)*lattice_constant,(len(self.atoms_pos_angstroms),1)) - np.tile(np.array(yy)*lattice_constant,(len(self.atoms_pos_angstroms),1))
        self.periodic_images_neg_a_pos_b = self.atoms_pos_angstroms - np.tile(np.array(xx)*lattice_constant,(len(self.atoms_pos_angstroms),1)) + np.tile(np.array(yy)*lattice_constant,(len(self.atoms_pos_angstroms),1))
        self.periodic_images_neg_a_neg_b = self.atoms_pos_angstroms - np.tile(np.array(xx)*lattice_constant,(len(self.atoms_pos_angstroms),1)) - np.tile(np.array(yy)*lattice_constant,(len(self.atoms_pos_angstroms),1))
        self.symbols = symbols # used in the element_enumerating function
        self.numbers = numbers
        self.total = total
        return

    def add_element_to_atom(self):
        self.elements_of_atoms = []
        if self.symbols == 'none' or self.numbers == 'none' or self.total == 'none':
            print("The method 'read_CONTCAR' must be run before the method 'add_element_to_atom' for this object.")
        for element_and_number in range(len(self.symbols)):
            element = self.symbols[element_and_number]
            number = self.numbers[element_and_number]
            for number_within_element in range(number):
                self.elements_of_atoms.append(element)
