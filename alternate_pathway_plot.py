import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.ticker import FormatStrFormatter
import numpy as np

def alternate_pathway_plot(dict_ads={}, dict_gas={}, T=723.15):
    if not bool(dict_ads):
        print('\nPlease provide the dict_ads dictionary.\n')
    if not bool(dict_gas):
        print('\nPlease provide the dict_gas dictionary.\n')

    Rh_CH4_gas_CO2_gas_1 = dict_ads['Rh_slab'][1] + dict_gas['CH4_gas'][1] + dict_gas['CO2_gas'][1]
    CH3_H_CO2_gas_2 = dict_ads['CH3_ads'][1] + dict_ads['H_ads'][1] + dict_gas['CO2_gas'][1] - dict_ads['Rh_slab'][1]
    CH2_2H_CO2_gas_3 = dict_ads['CH2_ads'][1] + 2*dict_ads['H_ads'][1] + dict_gas['CO2_gas'][1] - 2*dict_ads['Rh_slab'][1]
    CH2_CO2_gas_H2_gas_4 = dict_ads['CH2_ads'][1] + dict_gas['CO2_gas'][1] + dict_gas['H2_gas'][1]
    CH_H_CO2_gas_H2_gas_5 = dict_ads['CH_ads'][1] + dict_ads['H_ads'][1] + dict_gas['CO2_gas'][1] + dict_gas['H2_gas'][1] - dict_ads['Rh_slab'][1]
    C_2H_CO2_gas_H2_gas_6 = dict_ads['C_ads'][1] + 2*dict_ads['H_ads'][1] + dict_gas['CO2_gas'][1] + dict_gas['H2_gas'][1] - 2*dict_ads['Rh_slab'][1]
    C_CO2_gas_2H2_gas_7 = dict_ads['C_ads'][1] + dict_gas['CO2_gas'][1] + 2*dict_gas['H2_gas'][1]
    C_CO_O_2H2_gas_8 = dict_ads['C_ads'][1] + dict_ads['CO_ads'][1] + dict_ads['O_ads'][1] + 2*dict_gas['H2_gas'][1] - 2*dict_ads['Rh_slab'][1]
    C_O_2H2_gas_CO_gas_9 = dict_ads['C_ads'][1] + dict_ads['O_ads'][1] + 2*dict_gas['H2_gas'][1] + dict_gas['CO_gas'][1] - dict_ads['Rh_slab'][1]
    CO_2H2_gas_CO_gas_10 = dict_ads['CO_ads'][1] + 2*dict_gas['H2_gas'][1] + dict_gas['CO_gas'][1]
    Rh_2H2_gas_2CO_gas_11 = dict_ads['Rh_slab'][1] + 2*dict_gas['H2_gas'][1] + 2*dict_gas['CO_gas'][1]

    one = Rh_CH4_gas_CO2_gas_1 - Rh_CH4_gas_CO2_gas_1
    two = CH3_H_CO2_gas_2 - Rh_CH4_gas_CO2_gas_1
    three = CH2_2H_CO2_gas_3 - Rh_CH4_gas_CO2_gas_1
    four = CH2_CO2_gas_H2_gas_4 - Rh_CH4_gas_CO2_gas_1
    five = CH_H_CO2_gas_H2_gas_5 - Rh_CH4_gas_CO2_gas_1
    six = C_2H_CO2_gas_H2_gas_6 - Rh_CH4_gas_CO2_gas_1
    seven = C_CO2_gas_2H2_gas_7 - Rh_CH4_gas_CO2_gas_1
    eight = C_CO_O_2H2_gas_8 - Rh_CH4_gas_CO2_gas_1
    nine = C_O_2H2_gas_CO_gas_9 - Rh_CH4_gas_CO2_gas_1
    ten = CO_2H2_gas_CO_gas_10 - Rh_CH4_gas_CO2_gas_1
    eleven = Rh_2H2_gas_2CO_gas_11 - Rh_CH4_gas_CO2_gas_1

    one = np.sort(one)
    two = np.sort(two)
    three = np.sort(three)
    four = np.sort(four)
    five = np.sort(five)
    six = np.sort(six)
    seven = np.sort(seven)
    eight = np.sort(eight)
    nine = np.sort(nine)
    ten = np.sort(ten)
    eleven = np.sort(eleven)

    fig, ax = plt.subplots(figsize = (10,4))
    ax.tick_params(bottom=False)
    ax.plot(range(6),[one[999], two[999], three[999], four[999], five[999], six[999]],'b_', markersize=30, mew=2, label = 'expected value')
    ax.plot(range(6),[one[1899], two[1899], three[1899], four[1899], five[1899], six[1899]],'g_', markersize=30, mew=2,label = '95% confidence') #upper
    ax.plot(range(6),[one[99], two[99], three[99], four[99], five[99], six[99]],'g_', markersize=30, mew=2)
    ax.set_ylabel('Relative free energy (eV)')
    ax.legend(loc='upper left')
    fig.tight_layout()
    fig.savefig('pathway_alternate_part_1.png',dpi=220)

    fig1, ax1 = plt.subplots(figsize = (10,4))
    ax1.tick_params(bottom=False)
    ax1.set_xticks(np.arange(0, 5, step=1.0))
    ax1.yaxis.set_major_formatter(FormatStrFormatter('%.1f'))
    ax1.plot(range(5),[seven[999], eight[999], nine[999], ten[999], eleven[999]],'b_', markersize = 30, mew=2, label = 'expected value')
    ax1.plot(range(5),[seven[1899], eight[1899], nine[1899], ten[1899], eleven[1899]],'g_', markersize = 30, mew=2, label = '95% confidence')
    ax1.plot(range(5),[seven[99], eight[99], nine[99], ten[99], eleven[99]],'g_', markersize = 30, mew=2, label = '95% confidence')
    ax1.set_ylabel('Relative free energy (eV)')
    fig1.tight_layout()
    fig1.savefig('pathway_alternate_part_2.png',dpi=220)
