import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.ticker import FormatStrFormatter
import numpy as np

def pathway_plot(dict_ads={}, dict_gas={}, T=723.15):
    if not bool(dict_ads):
        print('\nPlease provide the dict_ads dictionary.\n')
    if not bool(dict_gas):
        print('\nPlease provide the dict_gas dictionary.\n')

    Rh_CH4_gas_CO2_gas_1 = dict_ads['Rh_slab'][1] + dict_gas['CH4_gas'][1] + dict_gas['CO2_gas'][1]
    CH3_H_CO2_gas_2 = dict_ads['CH3_ads'][1] + dict_ads['H_ads'][1] + dict_gas['CO2_gas'][1] - dict_ads['Rh_slab'][1]
    CH2_2H_CO2_gas_3 = dict_ads['CH2_ads'][1] + 2*dict_ads['H_ads'][1] + dict_gas['CO2_gas'][1] - 2*dict_ads['Rh_slab'][1]
    CH2_CO2_gas_H2_gas_4 = dict_ads['CH2_ads'][1] + dict_gas['CO2_gas'][1] + dict_gas['H2_gas'][1]
    CH_H_CO2_gas_H2_gas_5 = dict_ads['CH_ads'][1] + dict_ads['H_ads'][1] + dict_gas['CO2_gas'][1] + dict_gas['H2_gas'][1] - dict_ads['Rh_slab'][1]
    C_2H_CO2_gas_H2_gas_6 = dict_ads['C_ads'][1] + 2*dict_ads['H_ads'][1] + dict_gas['CO2_gas'][1] + dict_gas['H2_gas'][1] - 2*dict_ads['Rh_slab'][1]
    C_CO2_gas_2H2_gas_7 = dict_ads['C_ads'][1] + dict_gas['CO2_gas'][1] + 2*dict_gas['H2_gas'][1]
    C_CO_O_2H2_gas_8 = dict_ads['C_ads'][1] + dict_ads['CO_ads'][1] + dict_ads['O_ads'][1] + 2*dict_gas['H2_gas'][1] - 2*dict_ads['Rh_slab'][1]
    C_O_2H2_gas_CO_gas_9 = dict_ads['C_ads'][1] + dict_ads['O_ads'][1] + 2*dict_gas['H2_gas'][1] + dict_gas['CO_gas'][1] - dict_ads['Rh_slab'][1]
    CO_2H2_gas_CO_gas_10 = dict_ads['CO_ads'][1] + 2*dict_gas['H2_gas'][1] + dict_gas['CO_gas'][1]
    Rh_2H2_gas_2CO_gas_11 = dict_ads['Rh_slab'][1] + 2*dict_gas['H2_gas'][1] + 2*dict_gas['CO_gas'][1]

    Rh_CH4_gas_CO2_gas_1_ens = dict_ads['Rh_slab'][2] + dict_gas['CH4_gas'][2] + dict_gas['CO2_gas'][2]
    CH3_H_CO2_gas_2_ens = dict_ads['CH3_ads'][2] + dict_ads['H_ads'][2] + dict_gas['CO2_gas'][2] - dict_ads['Rh_slab'][2]
    CH2_2H_CO2_gas_3_ens = dict_ads['CH2_ads'][2] + 2*dict_ads['H_ads'][2] + dict_gas['CO2_gas'][2] - 2*dict_ads['Rh_slab'][2]
    CH2_CO2_gas_H2_gas_4_ens = dict_ads['CH2_ads'][2] + dict_gas['CO2_gas'][2] + dict_gas['H2_gas'][2]
    CH_H_CO2_gas_H2_gas_5_ens = dict_ads['CH_ads'][2] + dict_ads['H_ads'][2] + dict_gas['CO2_gas'][2] + dict_gas['H2_gas'][2] - dict_ads['Rh_slab'][2]
    C_2H_CO2_gas_H2_gas_6_ens = dict_ads['C_ads'][2] + 2*dict_ads['H_ads'][2] + dict_gas['CO2_gas'][2] + dict_gas['H2_gas'][2] - 2*dict_ads['Rh_slab'][2]
    C_CO2_gas_2H2_gas_7_ens = dict_ads['C_ads'][2] + dict_gas['CO2_gas'][2] + 2*dict_gas['H2_gas'][2]
    C_CO_O_2H2_gas_8_ens = dict_ads['C_ads'][2] + dict_ads['CO_ads'][2] + dict_ads['O_ads'][2] + 2*dict_gas['H2_gas'][2] - 2*dict_ads['Rh_slab'][2]
    C_O_2H2_gas_CO_gas_9_ens = dict_ads['C_ads'][2] + dict_ads['O_ads'][2] + 2*dict_gas['H2_gas'][2] + dict_gas['CO_gas'][2] - dict_ads['Rh_slab'][2]
    CO_2H2_gas_CO_gas_10_ens = dict_ads['CO_ads'][2] + 2*dict_gas['H2_gas'][2] + dict_gas['CO_gas'][2]
    Rh_2H2_gas_2CO_gas_11_ens = dict_ads['Rh_slab'][2] + 2*dict_gas['H2_gas'][2] + 2*dict_gas['CO_gas'][2]

    one = Rh_CH4_gas_CO2_gas_1 - Rh_CH4_gas_CO2_gas_1
    two = CH3_H_CO2_gas_2 - Rh_CH4_gas_CO2_gas_1
    three = CH2_2H_CO2_gas_3 - Rh_CH4_gas_CO2_gas_1
    four = CH2_CO2_gas_H2_gas_4 - Rh_CH4_gas_CO2_gas_1
    five = CH_H_CO2_gas_H2_gas_5 - Rh_CH4_gas_CO2_gas_1
    six = C_2H_CO2_gas_H2_gas_6 - Rh_CH4_gas_CO2_gas_1
    seven = C_CO2_gas_2H2_gas_7 - Rh_CH4_gas_CO2_gas_1
    eight = C_CO_O_2H2_gas_8 - Rh_CH4_gas_CO2_gas_1
    nine = C_O_2H2_gas_CO_gas_9 - Rh_CH4_gas_CO2_gas_1
    ten = CO_2H2_gas_CO_gas_10 - Rh_CH4_gas_CO2_gas_1
    eleven = Rh_2H2_gas_2CO_gas_11 - Rh_CH4_gas_CO2_gas_1

    one_std = np.std(Rh_CH4_gas_CO2_gas_1_ens - Rh_CH4_gas_CO2_gas_1_ens)
    two_std = np.std(CH3_H_CO2_gas_2_ens - Rh_CH4_gas_CO2_gas_1_ens)
    three_std = np.std(CH2_2H_CO2_gas_3_ens - Rh_CH4_gas_CO2_gas_1_ens)
    four_std = np.std(CH2_CO2_gas_H2_gas_4_ens - Rh_CH4_gas_CO2_gas_1_ens)
    five_std = np.std(CH_H_CO2_gas_H2_gas_5_ens - Rh_CH4_gas_CO2_gas_1_ens)
    six_std = np.std(C_2H_CO2_gas_H2_gas_6_ens - Rh_CH4_gas_CO2_gas_1_ens)
    seven_std = np.std(C_CO2_gas_2H2_gas_7_ens - Rh_CH4_gas_CO2_gas_1_ens)
    eight_std = np.std(C_CO_O_2H2_gas_8_ens - Rh_CH4_gas_CO2_gas_1_ens)
    nine_std = np.std(C_O_2H2_gas_CO_gas_9_ens - Rh_CH4_gas_CO2_gas_1_ens)
    ten_std = np.std(CO_2H2_gas_CO_gas_10_ens - Rh_CH4_gas_CO2_gas_1_ens)
    eleven_std = np.std(Rh_2H2_gas_2CO_gas_11_ens - Rh_CH4_gas_CO2_gas_1_ens)

    fig, ax = plt.subplots(figsize = (10,4))
    ax.tick_params(bottom=False)
    ax.plot(range(6),[one, two, three, four, five, six],'b_', markersize=30, mew=2, label = 'expected value')
    ax.plot(range(6),[one+2*one_std, two+2*two_std, three+2*three_std, four+2*four_std, five+2*five_std, six+2*six_std],'g_', markersize=30, mew=2,label = '95% confidence') #upper
    ax.plot(range(6),[one-2*one_std, two-2*two_std, three-2*three_std, four-2*four_std, five-2*five_std, six-2*six_std],'g_', markersize=30, mew=2)
    ax.set_ylabel('Relative free energy (eV)')
    ax.legend(loc='upper left')
    fig.tight_layout()
    fig.savefig('pathway_part_1.png',dpi=220)

    fig1, ax1 = plt.subplots(figsize = (10,4))
    ax1.tick_params(bottom=False)
    ax1.set_xticks(np.arange(0, 5, step=1.0))
    ax1.yaxis.set_major_formatter(FormatStrFormatter('%.1f'))
    ax1.plot(range(5),[seven, eight, nine, ten, eleven],'b_', markersize = 30, mew=2, label = 'expected value')
    ax1.plot(range(5),[seven+2*seven_std, eight+2*eight_std, nine+2*nine_std, ten+2*ten_std, eleven+2*eleven_std],'g_', markersize = 30, mew=2, label = '95% confidence')
    ax1.plot(range(5),[seven-2*seven_std, eight-2*eight_std, nine-2*nine_std, ten-2*ten_std, eleven-2*eleven_std],'g_', markersize = 30, mew=2)
    ax1.set_ylabel('Relative free energy (eV)')
    fig1.tight_layout()
    fig1.savefig('pathway_part_2.png',dpi=220)
