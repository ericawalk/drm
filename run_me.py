#Eric A. Walker
#This script reads calculation results and produces summaries and plots.
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.ticker import FormatStrFormatter
import numpy as np
import pandas as pd
import seaborn as sns; sns.set(style="ticks", color_codes=True)
from scipy.integrate import odeint
import sklearn
from sklearn.linear_model import Ridge
from sklearn.neural_network import MLPRegressor
import extract_egy
from extract_egy import extract_egy
import extract_egy_ensemble
from extract_egy_ensemble import extract_egy_ensemble
import free_egy_correct
from free_egy_correct import free_egy_correct
import NIST_entropy_calc
from NIST_entropy_calc import NIST_entropy_calc
import atoms
from atoms import atoms
import graph
from graph import graph
import get_descriptor
from get_descriptor import get_descriptor
import pathway_plot
from pathway_plot import pathway_plot
######################## Obtain free energies of DFT calculations:
T = 723.15
dict_ads = {
'Rh_slab': ['./converged_Rh_slab'],
'CH3_ads': ['./converged_CH3_ads'],
'CH2_ads' : ['./converged_CH2_ads'],
'CH_ads' : ['./converged_CH_ads'],
'C_ads' : ['./converged_C_ads'],
'H_ads' : ['./converged_H_ads'],
'O_ads' : ['./converged_O_ads'],
'OH_ads' : ['./converged_OH_ads'],
'CO_ads' : ['./converged_CO_ads']
}

dict_gas = {
'CH4_gas': ['./converged_CH4_gas'],
'CO2_gas': ['./converged_CO2_gas'],
'CO_gas': ['./converged_CO_gas'],
'H2_gas': ['./converged_H2_gas']
}

dict_struct = {
'CH2OH_ads': ['./structure_CH2OH_ads'],
'CH2O_ads': ['./structure_CH2O_ads'],
'CH3OH_ads': ['./structure_CH3OH_ads'],
'CH3O_ads': ['./structure_CH3O_ads'],
'COH_ads': ['./structure_COH_ads'],
'COOH_ads': ['./structure_COOH_ads'],
#'C_ads': ['./structure_C_ads'],
'HCOO_ads': ['./structure_HCOO_ads'],
'HCO_ads': ['./structure_HCO_ads'],
#'OH_ads': ['./structure_OH_ads']
#'O2_ads': ['./structure_O2_ads'],
#'O_ads': ['./structure_O_ads']
}

struct_rendering = [r'$CH_2OH$',r'$CH_2O$',r'$CH_3OH$',r'$CH_3O$',r'$COH$',r'$COOH$',r'$HCOO$',r'$HCO$']

for key in dict_ads:
    if key == 'Rh_slab':
        free_energy = np.asarray(extract_egy(dict_ads[key]))
        free_energy_ensemble = np.asarray(extract_egy_ensemble(dict_ads[key]))
        dict_ads[key].extend([free_energy,free_energy_ensemble])
    else:
        energy = np.asarray(extract_egy(dict_ads[key]))
        free_energy_ensemble = np.asarray(extract_egy_ensemble(dict_ads[key]))
        free_energy = energy + free_egy_correct(dict_ads[key])
        dict_ads[key].extend([free_energy,free_energy_ensemble])

CH4_gas_energy = np.asarray(extract_egy(dict_gas['CH4_gas']))
CH4_gas_free_energy_ensemble = np.asarray(extract_egy_ensemble(dict_gas['CH4_gas']))
CH4_gas_ZPE = free_egy_correct(dict_gas['CH4_gas'], ads_or_gas = 'gas')
CH4_gas_entropy = NIST_entropy_calc(T = T, A = -0.703029, B = 108.4773, C = -42.52157, D = 5.862788, E = 0.678565, F = -76.84376, G = 158.7163)
CH4_gas_free_energy = CH4_gas_energy + CH4_gas_ZPE - CH4_gas_entropy
dict_gas['CH4_gas'].extend([CH4_gas_free_energy, CH4_gas_free_energy_ensemble])

CO2_gas_energy = np.asarray(extract_egy(dict_gas['CO2_gas']))
CO2_gas_free_energy_ensemble = np.asarray(extract_egy_ensemble(dict_gas['CO2_gas']))
CO2_gas_ZPE = free_egy_correct(dict_gas['CO2_gas'], ads_or_gas = 'gas')
CO2_gas_entropy = NIST_entropy_calc(T = T, A = 24.99735, B = 55.18696, C = -33.69137, D = 7.948387, E = -0.136638, F = -403.6075, G = 228.2431)
CO2_gas_free_energy = CO2_gas_energy + CO2_gas_ZPE - CO2_gas_entropy
dict_gas['CO2_gas'].extend([CO2_gas_free_energy, CO2_gas_free_energy_ensemble])

CO_gas_energy = np.asarray(extract_egy(dict_gas['CO_gas']))
CO_gas_free_energy_ensemble = np.asarray(extract_egy_ensemble(dict_gas['CO_gas']))
CO_gas_ZPE = free_egy_correct(dict_gas['CO_gas'], ads_or_gas = 'gas')
CO_gas_entropy = NIST_entropy_calc(T = T, A = 25.56759, B = 6.096130, C = 4.054656, D = -2.671301, E = 0.131021, F = -118.0089, G = 227.3665)
CO_gas_free_energy = CO_gas_energy + CO_gas_ZPE - CO_gas_entropy
dict_gas['CO_gas'].extend([CO_gas_free_energy, CO_gas_free_energy_ensemble])

H2_gas_energy = np.asarray(extract_egy(dict_gas['H2_gas']))
H2_gas_free_energy_ensemble = np.asarray(extract_egy_ensemble(dict_gas['H2_gas']))
H2_gas_ZPE = free_egy_correct(dict_gas['H2_gas'], ads_or_gas = 'gas')
H2_gas_entropy = NIST_entropy_calc(T = T, A = 33.066178, B = -11.363417, C = 11.432816, D = -2.772874, E = -0.158558, F = -9.980797, G = 172.707974)
H2_gas_free_energy = H2_gas_energy + H2_gas_ZPE - H2_gas_entropy
dict_gas['H2_gas'].extend([H2_gas_free_energy, H2_gas_free_energy_ensemble])

# Construct free energy reaction pathway from DFT calculations and plot (figure 1)
Rh_CH4_gas_CO2_gas_1 = dict_ads['Rh_slab'][1] + dict_gas['CH4_gas'][1] + dict_gas['CO2_gas'][1]
CO_O_CH4_gas_2 = dict_ads['CO_ads'][1] + dict_ads['O_ads'][1] + dict_gas['CH4_gas'][1] - dict_ads['Rh_slab'][1]
CH3_H_CO_O_3 = dict_ads['CH3_ads'][1] + dict_ads['H_ads'][1] + dict_ads['CO_ads'][1] + dict_ads['O_ads'][1] - 3*dict_ads['Rh_slab'][1]
CH2_2H_CO_O_4 = dict_ads['CH2_ads'][1] + 2*dict_ads['H_ads'][1] + dict_ads['CO_ads'][1] + dict_ads['O_ads'][1] - 4*dict_ads['Rh_slab'][1]
CH_3H_CO_O_5 = dict_ads['CH_ads'][1] + 3*dict_ads['H_ads'][1] + dict_ads['CO_ads'][1] + dict_ads['O_ads'][1] - 5*dict_ads['Rh_slab'][1]
C_4H_CO_O_6 = dict_ads['C_ads'][1] + 4*dict_ads['H_ads'][1] + dict_ads['CO_ads'][1] + dict_ads['O_ads'][1] - 6*dict_ads['Rh_slab'][1]
fourH_2CO_7 = 4*dict_ads['H_ads'][1] + 2*dict_ads['CO_ads'][1] - 5*dict_ads['Rh_slab'][1]
fourH_CO_CO_gas_8 = 4*dict_ads['H_ads'][1] + dict_ads['CO_ads'][1] + dict_gas['CO_gas'][1] - 4*dict_ads['Rh_slab'][1] 
fourH_2CO_gas_9 = 4*dict_ads['H_ads'][1] + 2*dict_gas['CO_gas'][1] - 3*dict_ads['Rh_slab'][1]
twoH_2_CO_gas_H2_gas_10 = 2*dict_ads['H_ads'][1] + 2*dict_gas['CO_gas'][1] + dict_gas['H2_gas'][1] - dict_ads['Rh_slab'][1]
twoCO_gas_2H2_gas_11 = 2*dict_gas['CO_gas'][1] + 2*dict_gas['H2_gas'][1] + dict_ads['Rh_slab'][1]

Rh_CH4_gas_CO2_gas_1_ens = dict_ads['Rh_slab'][2] + dict_gas['CH4_gas'][2] + dict_gas['CO2_gas'][2]
CO_O_CH4_gas_2_ens = dict_ads['CO_ads'][2] + dict_ads['O_ads'][2] + dict_gas['CH4_gas'][2] - dict_ads['Rh_slab'][2]
CH3_H_CO_O_3_ens = dict_ads['CH3_ads'][2] + dict_ads['H_ads'][2] + dict_ads['CO_ads'][2] + dict_ads['O_ads'][2] - 3*dict_ads['Rh_slab'][2]
CH2_2H_CO_O_4_ens = dict_ads['CH2_ads'][2] + 2*dict_ads['H_ads'][2] + dict_ads['CO_ads'][2] + dict_ads['O_ads'][2] - 4*dict_ads['Rh_slab'][2]
CH_3H_CO_O_5_ens = dict_ads['CH_ads'][2] + 3*dict_ads['H_ads'][2] + dict_ads['CO_ads'][2] + dict_ads['O_ads'][2] - 5*dict_ads['Rh_slab'][2]
C_4H_CO_O_6_ens = dict_ads['C_ads'][2] + 4*dict_ads['H_ads'][2] + dict_ads['CO_ads'][2] + dict_ads['O_ads'][2] - 6*dict_ads['Rh_slab'][2]
fourH_2CO_7_ens = 4*dict_ads['H_ads'][2] + 2*dict_ads['CO_ads'][2] - 5*dict_ads['Rh_slab'][2]
fourH_CO_CO_gas_8_ens = 4*dict_ads['H_ads'][2] + dict_ads['CO_ads'][2] + dict_gas['CO_gas'][2] - 4*dict_ads['Rh_slab'][2]
fourH_2CO_gas_9_ens = 4*dict_ads['H_ads'][2] + 2*dict_gas['CO_gas'][2] - 3*dict_ads['Rh_slab'][2]
twoH_2_CO_gas_H2_gas_10_ens = 2*dict_ads['H_ads'][2] + 2*dict_gas['CO_gas'][2] + dict_gas['H2_gas'][2] - dict_ads['Rh_slab'][2]
twoCO_gas_2H2_gas_11_ens = 2*dict_gas['CO_gas'][2] + 2*dict_gas['H2_gas'][2] + dict_ads['Rh_slab'][2]

one = Rh_CH4_gas_CO2_gas_1 - Rh_CH4_gas_CO2_gas_1
two = CO_O_CH4_gas_2 - Rh_CH4_gas_CO2_gas_1
three = CH3_H_CO_O_3 - Rh_CH4_gas_CO2_gas_1
four = CH2_2H_CO_O_4 - Rh_CH4_gas_CO2_gas_1
five = CH_3H_CO_O_5 - Rh_CH4_gas_CO2_gas_1
six = C_4H_CO_O_6 - Rh_CH4_gas_CO2_gas_1
seven = fourH_2CO_7 - Rh_CH4_gas_CO2_gas_1
eight = fourH_CO_CO_gas_8 - Rh_CH4_gas_CO2_gas_1
nine = fourH_2CO_gas_9 - Rh_CH4_gas_CO2_gas_1
ten = twoH_2_CO_gas_H2_gas_10 - Rh_CH4_gas_CO2_gas_1
eleven = twoCO_gas_2H2_gas_11 - Rh_CH4_gas_CO2_gas_1

one_std = np.std(Rh_CH4_gas_CO2_gas_1_ens - Rh_CH4_gas_CO2_gas_1_ens)
two_std = np.std(CO_O_CH4_gas_2 - Rh_CH4_gas_CO2_gas_1)
three_std = np.std(CH3_H_CO_O_3 - Rh_CH4_gas_CO2_gas_1)
four_std = np.std(CH2_2H_CO_O_4 - Rh_CH4_gas_CO2_gas_1)
five_std = np.std(CH_3H_CO_O_5 - Rh_CH4_gas_CO2_gas_1)
six_std = np.std(C_4H_CO_O_6 - Rh_CH4_gas_CO2_gas_1)
seven_std = np.std(fourH_2CO_7 - Rh_CH4_gas_CO2_gas_1)
eight_std = np.std(fourH_CO_CO_gas_8 - Rh_CH4_gas_CO2_gas_1)
nine_std = np.std(fourH_2CO_gas_9 - Rh_CH4_gas_CO2_gas_1)
ten_std = np.std(twoH_2_CO_gas_H2_gas_10 - Rh_CH4_gas_CO2_gas_1)
eleven_std = np.std(twoCO_gas_2H2_gas_11 - Rh_CH4_gas_CO2_gas_1)

fig, ax = plt.subplots(figsize = (10,4))
ax.tick_params(bottom=False)
ax.plot(range(6),[one, two, three, four, five, six],'b_', markersize=30, mew=2, label = 'expected value')
ax.plot(range(6),[one+2*one_std, two+2*two_std, three+2*three_std, four+2*four_std, five+2*five_std, six+2*six_std],'g_', markersize=30, mew=2,label = '95% confidence') #upper
ax.plot(range(6),[one-2*one_std, two-2*two_std, three-2*three_std, four-2*four_std, five-2*five_std, six-2*six_std],'g_', markersize=30, mew=2)
ax.set_ylabel('Relative free energy (eV)')
ax.legend(loc='best')
fig.tight_layout()
fig.savefig('pathway_alternate_part_1.png',dpi=220)

fig1, ax1 = plt.subplots(figsize = (10,4))
ax1.tick_params(bottom=False)
ax1.set_xticks(np.arange(0, 5, step=1.0))
ax1.yaxis.set_major_formatter(FormatStrFormatter('%.1f'))
ax1.plot(range(5),[seven, eight, nine, ten, eleven],'b_', markersize = 30, mew=2, label = 'expected value')
ax1.plot(range(5),[seven+2*seven_std, eight+2*eight_std, nine+2*nine_std, ten+2*ten_std, eleven+2*eleven_std],'g_', markersize = 30, mew=2, label = '95% confidence')
ax1.plot(range(5),[seven-2*seven_std, eight-2*eight_std, nine-2*nine_std, ten-2*ten_std, eleven-2*eleven_std],'g_', markersize = 30, mew=2)
ax1.set_ylabel('Relative free energy (eV)')
fig1.tight_layout()
fig1.savefig('pathway_alternate_part_2.png',dpi=220)

pathway_plot(dict_ads = dict_ads, dict_gas = dict_gas, T=T)

############################ Obtain descriptor for dictionary of adsorbates
# create referenced and sorted free energies
dict_ads['Rh_slab'].append(one)
dict_ads['CH3_ads'].append(dict_ads['CH3_ads'][1] + dict_ads['H_ads'][1] + dict_gas['CO2_gas'][1] - dict_ads['Rh_slab'][1] - Rh_CH4_gas_CO2_gas_1)
dict_ads['CH2_ads'].append(dict_ads['CH2_ads'][1] + dict_gas['H2_gas'][1] + dict_gas['CO2_gas'][1] - Rh_CH4_gas_CO2_gas_1)
dict_ads['CH_ads'].append(dict_ads['CH_ads'][1] + dict_gas['H2_gas'][1] + dict_ads['H_ads'][1] + dict_gas['CO2_gas'][1] - dict_ads['Rh_slab'][1]- Rh_CH4_gas_CO2_gas_1)
dict_ads['C_ads'].append(dict_ads['C_ads'][1] + 2*dict_gas['H2_gas'][1] + dict_gas['CO2_gas'][1] - Rh_CH4_gas_CO2_gas_1)
dict_ads['H_ads'].append(dict_ads['H_ads'][1] + 2*dict_gas['CO_gas'][1] + dict_gas['H2_gas'][1] + dict_ads['H_ads'][1] - dict_ads['Rh_slab'][1] - Rh_CH4_gas_CO2_gas_1)
dict_ads['O_ads'].append(dict_ads['O_ads'][1] + dict_gas['CO_gas'][1] + dict_gas['CH4_gas'][1] - Rh_CH4_gas_CO2_gas_1)
#dict_ads['OH_ads'].append(dict_ads['OH_ads'][1] + dict_gas['CO_gas'][1] + 1.5*dict_gas['H2_gas'][1] + dict_ads['C_ads'][1] - dict_ads['Rh_slab'][1] - Rh_CH4_gas_CO2_gas_1)
dict_ads['OH_ads'].append(dict_ads['OH_ads'][1] + dict_ads['CO_ads'][1] + dict_ads['C_ads'][1] + dict_ads['H_ads'][1] + dict_gas['H2_gas'][1] - 3*dict_ads['Rh_slab'][1] - Rh_CH4_gas_CO2_gas_1)
dict_ads['CO_ads'].append(dict_ads['CO_ads'][1] + dict_ads['O_ads'][1] + dict_gas['CH4_gas'][1] - dict_ads['Rh_slab'][1] - Rh_CH4_gas_CO2_gas_1)

# The standard deviations for making confidence intervals:
dict_ads['Rh_slab'].append(one_std)
dict_ads['CH3_ads'].append(np.std(dict_ads['CH3_ads'][2] + dict_ads['H_ads'][2] + dict_gas['CO2_gas'][2] - dict_ads['Rh_slab'][2] - Rh_CH4_gas_CO2_gas_1_ens))
dict_ads['CH2_ads'].append(np.std(dict_ads['CH2_ads'][2] + dict_gas['H2_gas'][2] + dict_gas['CO2_gas'][2] - Rh_CH4_gas_CO2_gas_1_ens))
dict_ads['CH_ads'].append(np.std(dict_ads['CH_ads'][2] + dict_gas['H2_gas'][2] + dict_ads['H_ads'][2] + dict_gas['CO2_gas'][2] - dict_ads['Rh_slab'][2]- Rh_CH4_gas_CO2_gas_1_ens))
dict_ads['C_ads'].append(np.std(dict_ads['C_ads'][2] + 2*dict_gas['H2_gas'][2] + dict_gas['CO2_gas'][2] - Rh_CH4_gas_CO2_gas_1_ens))
dict_ads['H_ads'].append(np.std(dict_ads['H_ads'][2] + 2*dict_gas['CO_gas'][2] + dict_gas['H2_gas'][2] + dict_ads['H_ads'][2] - dict_ads['Rh_slab'][2] - Rh_CH4_gas_CO2_gas_1_ens))
dict_ads['O_ads'].append(np.std(dict_ads['O_ads'][2] + dict_gas['CO_gas'][2] + dict_gas['CH4_gas'][2] - Rh_CH4_gas_CO2_gas_1_ens))
#dict_ads['OH_ads'].append(np.std(dict_ads['OH_ads'][2] + dict_gas['CO_gas'][2] + 1.5*dict_gas['H2_gas'][2] + dict_ads['C_ads'][2] - dict_ads['Rh_slab'][2] - Rh_CH4_gas_CO2_gas_1_ens))
dict_ads['OH_ads'].append(np.std(dict_ads['OH_ads'][2] + dict_ads['CO_ads'][2] + dict_ads['C_ads'][2] + dict_ads['H_ads'][2] + dict_gas['H2_gas'][2] - 3*dict_ads['Rh_slab'][2] - Rh_CH4_gas_CO2_gas_1_ens))
dict_ads['CO_ads'].append(np.std(dict_ads['CO_ads'][2] + dict_ads['O_ads'][2] + dict_gas['CH4_gas'][2] - dict_ads['Rh_slab'][2] - Rh_CH4_gas_CO2_gas_1_ens))

X = [] #descriptors
y = [] #DFT free energies
y_up_95 = [] #upper 95% confidence
y_low_95 = []
atoms_obj = []
graph_obj = []
for k,key in enumerate(dict_ads):
    atoms_obj.append(atoms(key))
    atoms_obj[k].read_CONTCAR(dict_ads[key][0] + '/CONTCAR')
    atoms_obj[k].add_element_to_atom()
    graph_obj.append(graph(atoms_obj[k]))
    graph_obj[k].prepare_csv_for_gephi_graph(key)
    graph_obj[k].prepare_csv_for_gephi_graph_second_neighbors(filename_prefix=key)
    descriptor = get_descriptor(graph_obj[k])
    print('\n', key, ': descriptor: \n',descriptor)
    X.append(descriptor)
    y.append(dict_ads[key][3])
    y_up_95.append(dict_ads[key][3] + 2*dict_ads[key][3])
    y_low_95.append(dict_ads[key][3] - 2*dict_ads[key][3])
    print('\n', key, 'y: ', dict_ads[key][3])
    print('\n', key, 'y_up_95: ', dict_ads[key][3] + 2*dict_ads[key][3])
    print('\n', key, 'y_low_95: ', dict_ads[key][3] - 2*dict_ads[key][3])
del atoms_obj
del graph_obj

X = np.vstack(X) 
y = np.asarray(y)
y_up_95 = np.asarray(y_up_95)
y_low_95 = np.asarray(y_low_95)

X_structures = []
atoms_struct_obj = []
graph_struct_obj = []
for k,key in enumerate(dict_struct):
    atoms_struct_obj.append(atoms(key))
    atoms_struct_obj[k].read_CONTCAR(dict_struct[key][0] + '/POSCAR')
    atoms_struct_obj[k].add_element_to_atom()
    graph_struct_obj.append(graph(atoms_struct_obj[k]))
    graph_struct_obj[k].prepare_csv_for_gephi_graph(key)
    graph_struct_obj[k].prepare_csv_for_gephi_graph_second_neighbors(filename_prefix=key)
    descriptor = get_descriptor(graph_struct_obj[k])
    print('\n', key, ': descriptor: \n',descriptor)
    X_structures.append(descriptor)
del atoms_struct_obj
del graph_struct_obj
X_structures = np.vstack(X_structures)
print('X: ',X)
print('X_structures: ', X_structures)
print('y: ',y)
print('y_up_95: ', y_up_95)
print('y_low_95: ', y_low_95)
reg = Ridge().fit(X, y) #MLPRegressor(max_iter=500)
egy_pred = reg.predict(X_structures)
reg_up_95 = Ridge().fit(X, y_up_95)
egy_pred_up_95 = reg_up_95.predict(X_structures)
reg_low_95 = Ridge().fit(X, y_low_95)
egy_pred_low_95 = reg_low_95.predict(X_structures)

#ref_CH2OH_ads = np.sort(dict_ads['H_ads'][1] + dict_ads['CO_ads'][1] - 2*dict_ads['Rh_slab'][1])
#ref_CH2O_ads = np.sort(2*dict_ads['H_ads'][1] + dict_ads['CO_ads'][1] - 3*dict_ads['Rh_slab'][1])
#ref_CH3OH_ads = np.sort(dict_ads['CO_ads'][1] - dict_ads['Rh_slab'][1])
#ref_CH3O_ads = np.sort(dict_ads['H_ads'][1] + dict_ads['CO_ads'][1] - 2*dict_ads['Rh_slab'][1])
#ref_COH_ads = np.sort(dict_ads['CH3_ads'][1] + dict_ads['O_ads'][1] - 2*dict_ads['Rh_slab'][1])
#ref_COOH_ads = np.sort(dict_ads['CH3_ads'][1] - dict_ads['Rh_slab'][1])
#ref_HCOO_ads = np.sort(dict_ads['CH3_ads'][1] - dict_ads['Rh_slab'][1])
#ref_HCO_ads = np.sort(dict_ads['CH3_ads'][1] + dict_ads['O_ads'][1] - 2*dict_ads['Rh_slab'][1])
#ref_OH_ads = np.sort(dict_gas['CO_gas'][1] + dict_gas['H2_gas'][1] + dict_ads['CO_ads'][1] + dict_ads['H_ads'][1] - 2*dict_ads['Rh_slab'][1])#np.sort(dict_gas['CH4_gas'][1] + dict_gas['CO_gas'][1] - dict_ads['O_ads'][1] - dict_ads['H_ads'][1] + 2*dict_ads['Rh_slab'][1])#np.sort(dict_ads['CH3_ads'][1] + dict_ads['CO_ads'][1] - 2*dict_ads['Rh_slab'][1])

#list_of_references = [ref_CH2OH_ads, ref_CH2O_ads, ref_CH3OH_ads, ref_CH3O_ads, ref_COH_ads, ref_COOH_ads, ref_HCOO_ads, ref_HCO_ads, ref_OH_ads]

#for i in range(len(list_of_references)):
#    egy_pred[i] = egy_pred[i] + list_of_references[i][999]
#    egy_pred_up_95[i] = egy_pred_up_95[i] + list_of_references[i][1899]
#    egy_pred_low_95[i] = egy_pred_low_95[i] + list_of_references[i][99]

fig2, ax2 = plt.subplots(figsize=(10, 4))
ax2.tick_params(bottom=False)
ax2.set_xticks(np.arange(0, 12, step=1.0))
ax2.set_xticklabels(struct_rendering)
ax2.plot(range(8),egy_pred,'b_', markersize = 30, mew=2, label = 'expected value')
ax2.plot(range(8),egy_pred_up_95,'g_', markersize = 30, mew=2, label = '95% confidence')
ax2.plot(range(8),egy_pred_low_95,'g_', markersize = 30, mew=2)
ax2.set_ylabel('Relative free energy (eV)')
ax2.legend(loc='best')
ax2.yaxis.set_major_formatter(FormatStrFormatter('%.1f'))
fig2.tight_layout()
fig2.savefig('predictions.png',dpi=220)

back_pred = reg.predict(X)
fig3, ax3 = plt.subplots(figsize=(5, 5))
#ax3.set_xticklabels([r'$Rh(111)$',r'$CH_3$',r'$CH_2$',r'$CH$',r'$C$',r'$H$',r'$O$',r'$OH$',r'$CO$'])
ax3.plot([0., 1.8], [0., 1.8], color = 'k', label = 'parity')
ax3.scatter(y[0],back_pred[0],label = r'$Rh(111)$', color = 'tab:blue')
ax3.scatter(y[1],back_pred[1],label = r'$CH_3$', color = 'tab:orange')
ax3.scatter(y[2],back_pred[2],label = r'$CH_2$', color = 'tab:green')
ax3.scatter(y[3],back_pred[3],label = r'$CH$', color = 'tab:red')
ax3.scatter(y[4],back_pred[4],label = r'$C$', color = 'tab:purple')
ax3.scatter(y[5],back_pred[5],label = r'$H$', color = 'tab:brown')
ax3.scatter(y[6],back_pred[6],label = r'$O$', color = 'tab:pink')
ax3.scatter(y[7],back_pred[7],label = r'$OH$', color = 'tab:gray')
ax3.scatter(y[8],back_pred[8],label = r'$CO$', color = 'tab:olive')
ax3.legend(loc='best')
ax3.set_xlabel('DFT training')
ax3.set_ylabel('back predicted')
fig3.tight_layout()
fig3.savefig('back_predictions.png',dpi=220)
