import numpy as np

adj = np.array([[0, 0, 0, 0, 0, 1],
                [0, 0, 0, 0, 0, 1],
                [0, 0, 0, 0, 0, 1],
                [0, 0, 0, 0, 1, 0],
                [0, 0, 0, 1, 0, 1],
                [1, 1, 1, 0, 1, 0]])

adj_sqr = np.matmul(adj,adj)
print('adj_sqr \n',adj_sqr)

adj = np.array([[0, 1, 1, 1],
                [1, 0, 0, 0],
                [1, 0, 0, 0],
                [1, 0, 0, 0]])

adj_sqr = np.matmul(adj,adj)
print('adj_sqr \n',adj_sqr)

