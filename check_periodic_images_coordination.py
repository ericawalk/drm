import numpy as np
def check_periodic_images_coordination(i=[], j=[], point_i=[], point_j=[],bond_threshold_length=1.5, periodic_images_dict = {}, bonded=False):
    for key in periodic_images_dict:
        periodic_point_j = periodic_images_dict[key][j,:]
        euclidean_distance_i_j = np.sqrt(np.sum(np.square(periodic_point_j-point_i)))
        if euclidean_distance_i_j < bond_threshold_length:
            bonded=True        
    return bonded
