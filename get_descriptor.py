import numpy as np
def get_descriptor(graph_object):
    descriptor = np.zeros(22)
    descriptor[0] = graph_object.num_C
    descriptor[1] = graph_object.num_O
    descriptor[2] = graph_object.num_H
    descriptor[3] = graph_object.num_other
    descriptor[4] = graph_object.num_C_C
    descriptor[5] = graph_object.num_C_O
    descriptor[6] = graph_object.num_C_H
    descriptor[7] = graph_object.num_O_O
    descriptor[8] = graph_object.num_O_H
    descriptor[9] = graph_object.num_H_H
    descriptor[10] = graph_object.num_C_other
    descriptor[11] = graph_object.num_O_other
    descriptor[12] = graph_object.num_H_other
    descriptor[13] = graph_object.num_C_next_C
    descriptor[14] = graph_object.num_C_next_O
    descriptor[15] = graph_object.num_C_next_H
    descriptor[16] = graph_object.num_O_next_O
    descriptor[17] = graph_object.num_O_next_H
    descriptor[18] = graph_object.num_H_next_H
    descriptor[19] = graph_object.num_C_next_other
    descriptor[20] = graph_object.num_O_next_other
    descriptor[21] = graph_object.num_H_next_other
    return descriptor
