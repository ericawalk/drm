import numpy as np
import pandas as pd
import check_periodic_images_coordination
from check_periodic_images_coordination import check_periodic_images_coordination
# This class contains methods to prepare the input files for a gephi graph. EAW 2020/01/13
class graph:

    def __init__(self,atoms_object_name = 'none'): # The second argument is the atoms object instance that will be operated.
        if atoms_object_name == 'none':
            print("Pass an atoms_object_name when initiating a graph object instance.")
            return
        self.atoms_object_name = atoms_object_name
        self.total = 0
        self.total = getattr(atoms_object_name,'total')
        self.symbols = getattr(atoms_object_name,'symbols')
        self.numbers = getattr(atoms_object_name,'numbers')
        self.elements_of_atoms = []
        self.elements_of_atoms = getattr(atoms_object_name,'elements_of_atoms')
        self.atoms_pos_angstroms = getattr(atoms_object_name,'atoms_pos_angstroms')
        self.periodic_images_positive_a = getattr(atoms_object_name,'periodic_images_positive_a')
        self.periodic_images_negative_a = getattr(atoms_object_name,'periodic_images_negative_a')
        self.periodic_images_positive_b = getattr(atoms_object_name,'periodic_images_positive_b')
        self.periodic_images_negative_b = getattr(atoms_object_name,'periodic_images_negative_b')
        self.periodic_images_pos_a_pos_b = getattr(atoms_object_name,'periodic_images_pos_a_pos_b')
        self.periodic_images_pos_a_neg_b = getattr(atoms_object_name,'periodic_images_pos_a_neg_b')
        self.periodic_images_neg_a_pos_b = getattr(atoms_object_name,'periodic_images_neg_a_pos_b')
        self.periodic_images_neg_a_neg_b = getattr(atoms_object_name,'periodic_images_neg_a_neg_b')
        self.all_periodic_images = np.vstack((self.periodic_images_positive_a, self.periodic_images_negative_a, self.periodic_images_positive_b, self.periodic_images_negative_b, self.periodic_images_pos_a_pos_b, self.periodic_images_pos_a_neg_b, self.periodic_images_neg_a_pos_b, self.periodic_images_neg_a_neg_b))
        self.periodic_images_dict = {
	'periodic_images_positive_a': self.periodic_images_positive_a,
        'periodic_images_negative_a': self.periodic_images_negative_a,
	'periodic_images_positive_b': self.periodic_images_positive_b,
	'periodic_images_negative_b': self.periodic_images_negative_b,
	'periodic_images_pos_a_pos_b': self.periodic_images_pos_a_pos_b,
	'periodic_images_pos_a_neg_b': self.periodic_images_pos_a_neg_b,
	'periodic_images_neg_a_pos_b': self.periodic_images_neg_a_pos_b,
	'periodic_images_neg_a_neg_b': self.periodic_images_neg_a_neg_b
	}
        self.num_C = 0
        self.num_O = 0
        self.num_H = 0
        self.num_other = 0
        for element in self.elements_of_atoms:
            if element == 'C':
                self.num_C = self.num_C + 1
            elif element == 'O':
                self.num_O = self.num_O + 1
            elif element == 'H':
                self.num_H = self.num_H + 1
            else:
                self.num_other = self.num_other + 1  
        self.euclidean_distance_matrix = np.empty((self.total,self.total))
        self.edge_source_list = []
        self.edge_target_list = []
        self.adjacency = []
        self.adjacency_squared = []
        self.adjacency = np.zeros((self.total,self.total))
        self.adjacency_squared = np.zeros((self.total,self.total))
        self.num_C_C = 0
        self.num_C_O = 0
        self.num_C_H = 0
        self.num_C_other = 0
        self.num_O_O = 0
        self.num_O_H = 0
        self.num_O_other = 0
        self.num_H_H = 0
        self.num_H_other = 0
        self.next_nearest_edge_source_list = []
        self.next_nearest_edge_target_list = []
        self.num_C_next_C = 0
        self.num_C_next_O = 0
        self.num_C_next_H = 0
        self.num_C_next_other = 0
        self.num_O_next_O = 0
        self.num_O_next_H = 0
        self.num_O_next_other = 0
        self.num_H_next_H = 0
        self.num_H_next_other = 0
        def __del__(self):
            print('deleted')

    def get_edge_list(self, bond_threshold_length = 1.5, adsorption_bond_threshold_length = 2.5, within_catalyst_bond_threshold_length = 2.8):
        self.adjacency = []
        self.adjacency_squared = []
        self.adjacency = np.zeros((self.total,self.total))
        self.adjacency_squared = np.zeros((self.total,self.total))
        for i in range(self.total): # The row index will be 'i'.
            point_i = self.atoms_pos_angstroms[i,:]            
            for j in range(i,self.total): # The column index will be 'j'.  By starting at 'i' here, repeat calculations are avoided because the distance matrix is symmetric.  I.e., the distance at i,j is equal to the distance at j,i.
                point_j = self.atoms_pos_angstroms[j,:]
                euclidean_distance_i_j = np.sqrt(np.sum(np.square(point_j-point_i)))
                self.euclidean_distance_matrix[i,j] = euclidean_distance_i_j
                self.euclidean_distance_matrix[j,i] = euclidean_distance_i_j
                could_be_adsorption = self.elements_of_atoms[i] in ['C','O','H'] and not self.elements_of_atoms[j] in ['C','O','H']
                could_be_adsorption_reverse = self.elements_of_atoms[j] in ['C','O','H'] and not self.elements_of_atoms[i] in ['C','O','H']
                bool_coordination = (euclidean_distance_i_j <= adsorption_bond_threshold_length) and any([could_be_adsorption, could_be_adsorption_reverse])
                #print('could_be_adsorption',could_be_adsorption,'could_be_adsorption_reverse',could_be_adsorption_reverse,'euclidean distance', euclidean_distance_i_j <= adsorption_bond_threshold_length, 'bool_coordination',bool_coordination)
                bool_periodic_images = check_periodic_images_coordination(i=i, j=j, point_i=point_i, point_j=point_j,bond_threshold_length=adsorption_bond_threshold_length, periodic_images_dict = self.periodic_images_dict)
                bool_euclidean = euclidean_distance_i_j <= bond_threshold_length and not i==j
                bool_within_catalyst = euclidean_distance_i_j <= within_catalyst_bond_threshold_length and not i==j
                if any([bool_coordination, bool_periodic_images, bool_euclidean, bool_within_catalyst]):
                    self.edge_source_list.append(i)
                    self.edge_target_list.append(j)
                    self.adjacency[i,j] = 1
                    self.adjacency[j,i] = 1
                    if self.elements_of_atoms[i] == 'C' and self.elements_of_atoms[j] == 'C':
                        self.num_C_C = self.num_C_C + 1
                    if self.elements_of_atoms[i] == 'C' and self.elements_of_atoms[j] == 'O':
                        self.num_C_O = self.num_C_O + 1
                    if self.elements_of_atoms[i] == 'C' and not self.elements_of_atoms[j] in ['C','O','H']:
                        self.num_C_other = self.num_C_other + 1
                    if self.elements_of_atoms[j] == 'C' and not self.elements_of_atoms[i] in ['C','O','H']: #i and j switched
                        self.num_C_other = self.num_C_other + 1
                    if self.elements_of_atoms[i] == 'O' and self.elements_of_atoms[j] == 'C':
                        self.num_C_O = self.num_C_O + 1
                    if self.elements_of_atoms[i] == 'C' and self.elements_of_atoms[j] == 'H':
                        self.num_C_H = self.num_C_H + 1
                    if self.elements_of_atoms[i] == 'H' and self.elements_of_atoms[j] == 'C':
                        self.num_C_H = self.num_C_H + 1
                    if self.elements_of_atoms[i] == 'O' and self.elements_of_atoms[j] == 'O':
                        self.num_O_O = self.num_O_O + 1
                    if self.elements_of_atoms[i] == 'O' and self.elements_of_atoms[j] == 'H':
                        self.num_O_H = self.num_O_H + 1
                    if self.elements_of_atoms[i] == 'O' and not self.elements_of_atoms[j] in ['C','O','H']:
                        self.num_O_other = self.num_O_other + 1
                    if self.elements_of_atoms[j] == 'O' and not self.elements_of_atoms[i] in ['C','O','H']: #i and j switched
                        self.num_O_other = self.num_O_other + 1
                    if self.elements_of_atoms[i] == 'H' and self.elements_of_atoms[j] == 'O':
                        self.num_O_H = self.num_O_H + 1
                    if self.elements_of_atoms[i] == 'H' and self.elements_of_atoms[j] == 'H':
                        self.num_H_H = self.num_H_H + 1
                    if self.elements_of_atoms[i] == 'H' and not self.elements_of_atoms[j] in ['C','O','H']:
                        self.num_H_other = self.num_H_other + 1
                    if self.elements_of_atoms[j] == 'H' and not self.elements_of_atoms[i] in ['C','O','H']: #i and j switched
                        self.num_H_other = self.num_H_other + 1
        self.edge_type_list = ['Undirected']*len(self.edge_source_list)
        self.edge_weight_list = [1]*len(self.edge_source_list)

    def prepare_csv_for_gephi_graph(self, filename_prefix = ''):
        if self.atoms_object_name == 'none':
            print("The keyword argument 'atoms_object_name=' must be passed to this method.")
            print("Better, pass the atoms_object_name when creating an object instance of graph.")
            return # The method stops
        if getattr(self.atoms_object_name, 'total') == 'none':
            print("The 'read_CONTCAR' method of the atoms object should be run first.")
            return
        if len(getattr(self.atoms_object_name, 'elements_of_atoms')) == 0:
            print("Run the 'add_element_to_atom' method on the atoms object, first.")
            return
        if filename_prefix == '':
            print("Warning: please set filename prefix for graph .csv files.")

        node_txt = pd.DataFrame(columns = ['element'])
        node_txt['element'] = self.elements_of_atoms
        node_txt.to_csv(filename_prefix + '_nodes.csv', index_label='Id') # The 'Id' label is for importing the data to the Gephi software.        
        self.get_edge_list()
        edge_txt = pd.DataFrame(columns = ['Source','Target','Type'])
        edge_txt['Source'] = self.edge_source_list
        edge_txt['Target'] = self.edge_target_list
        edge_txt['Type'] = self.edge_type_list
        edge_txt.to_csv(filename_prefix + '_edges.csv', index=False) # The edges do not have indices.  They refer to the node indices.

    def prepare_csv_for_gephi_graph_second_neighbors(self, filename_prefix = ''):
        df = pd.DataFrame(self.adjacency)
        filepath = filename_prefix + '_adjacency.xlsx'
        df.to_excel(filepath, index=False)
        self.adjacency_squared = []
        self.adjacency_squared = np.zeros((self.total,self.total))
        self.adjacency_squared = np.matmul(self.adjacency, self.adjacency)
        print('adjacency',self.adjacency,'adjacency squared', self.adjacency_squared)
        for i in range(self.total):
            for j in range(i,self.total):
                if self.adjacency_squared[i,j] > 0.99 and not i==j: #testing for zero is not a good idea because "zero" could be 1e-308 and could return as "not zero".
                    self.next_nearest_edge_source_list.append(i)
                    self.next_nearest_edge_target_list.append(j)
                    if self.elements_of_atoms[i] == 'C' and self.elements_of_atoms[j] == 'C':
                        self.num_C_next_C = self.num_C_next_C + 1
                    if self.elements_of_atoms[i] == 'C' and self.elements_of_atoms[j] == 'O':
                        self.num_C_next_O = self.num_C_next_O + 1
                    if self.elements_of_atoms[i] == 'C' and not self.elements_of_atoms[j] in ['C','O','H']:
                        self.num_C_next_other = self.num_C_next_other + 1
                    if self.elements_of_atoms[j] == 'C' and not self.elements_of_atoms[i] in ['C','O','H']: #i and j switched
                        self.num_C_next_other = self.num_C_next_other + 1
                    if self.elements_of_atoms[i] == 'O' and self.elements_of_atoms[j] == 'C':
                        self.num_C_next_O = self.num_C_next_O + 1
                    if self.elements_of_atoms[i] == 'C' and self.elements_of_atoms[j] == 'H':
                        self.num_C_next_H = self.num_C_next_H + 1
                    if self.elements_of_atoms[i] == 'H' and self.elements_of_atoms[j] == 'C':
                        self.num_C_next_H = self.num_C_next_H + 1
                    if self.elements_of_atoms[i] == 'O' and self.elements_of_atoms[j] == 'O':
                        self.num_O_next_O = self.num_O_next_O + 1
                    if self.elements_of_atoms[i] == 'O' and self.elements_of_atoms[j] == 'H':
                        self.num_O_next_H = self.num_O_next_H + 1
                    if self.elements_of_atoms[i] == 'O' and not self.elements_of_atoms[j] in ['C','O','H']:
                        self.num_O_next_other = self.num_O_next_other + 1
                    if self.elements_of_atoms[j] == 'O' and not self.elements_of_atoms[i] in ['C','O','H']: #i and j switched
                        self.num_O_next_other = self.num_O_next_other + 1
                    if self.elements_of_atoms[i] == 'H' and self.elements_of_atoms[j] == 'O':
                        self.num_O_next_H = self.num_O_next_H + 1                    
                    if self.elements_of_atoms[i] == 'H' and self.elements_of_atoms[j] == 'H':
                        self.num_H_next_H = self.num_H_next_H + 1
                    if self.elements_of_atoms[i] == 'H' and not self.elements_of_atoms[j] in ['C','O','H']:
                        self.num_H_next_other = self.num_H_next_other + 1
                    if self.elements_of_atoms[j] == 'H' and not self.elements_of_atoms[i] in ['C','O','H']: #i and j switched
                        self.num_H_next_other = self.num_H_next_other + 1
                        #print('i',i,' j',j)
        self.edge_weight_list.extend([0.5]*len(self.next_nearest_edge_target_list))
        df_squared = pd.DataFrame(self.adjacency_squared)
        filepath_squared = filename_prefix + '_adjacency_squared.xlsx'
        df_squared.to_excel(filepath_squared, index=False) 
