from ase import Atoms
from ase.io import write,read
from ase.lattice.surface import fcc111, add_adsorbate
from ase.constraints import FixAtoms


# Generate the representation of the atoms in ASE
slab = fcc111('Rh', size=(3,4,4), vacuum=15.0)
molecule = Atoms('O', [(0., 0., 0.)])
add_adsorbate(slab, molecule, 0.3, 'fcc',offset=2)
mask = [atom.tag > 2 for atom in slab]
slab.set_constraint(FixAtoms(mask=mask))
write('POSCAR',slab)


