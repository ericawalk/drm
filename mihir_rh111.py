import atoms
from atoms import atoms
import graph
from graph import graph
import bag_of_bonds_descriptor
from bag_of_bonds_descriptor import bag_of_bonds_descriptor
Rh111_atoms = atoms()
Rh111_atoms.read_CONTCAR('S1/CONTCAR')
Rh111_atoms.add_element_to_atom()
Rh111_graph = graph(Rh111_atoms)
Rh111_graph.prepare_csv_for_gephi_graph('Rh111')
#CO_O_graph.prepare_csv_for_gephi_graph_second_neighbors('CO_O')
completed_structs_dict  = {'Rh111': './S1', \
'CO2_free': './CO2_vacuum', \
'CH4_free' : './CH4_vacuum', \
'CO_O' : './CO2_ads', \
'CH3' : './CH3_ads', \
'H' : './H_ads', \
'CH2_H' : './CH2_H', \
'CH' : './CH_ads', \
'H2_free' : './H2_free', \
'C' : './C_ads', \
'CO' : './CO_ads', \
'CO_free' : './CO_vacuum'} # The CONTCAR files are located in this dictionary's entries.  Name the csv files, e.g. object_instance.prepare_csv_for_gephi_graph('name'), where the name comes from the dictionary keys.
bag_of_bonds_DRM = bag_of_bonds_descriptor(completed_structs_dict)